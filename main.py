from source.handler.commands import *
from source.handler.messages import *
from aiogram import executor


if __name__ == "__main__":
    executor.start_polling(dp)
