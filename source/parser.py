import requests
from bs4 import BeautifulSoup


def get_new_post():
    headers = {
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:45.0) Gecko/20100101 Firefox/45.0"
    }
    url = "https://gymnasia.org/"
    r = requests.get(url, headers=headers)
    soup = BeautifulSoup(r.text, features="html.parser")

    all_posts = soup.find_all("div", {"class": "post"})
    for post in all_posts:
        header = post.find("h3").get_text()
        text = post.find("div", {"class": "text"}).get_text()

        ans = ""
        ans += f"**{header}.**\n\n"
        ans += text + "\n\n"
        for i in range(0, len(ans), 1024):
            yield ans[i : i + 1024]


4



if __name__ == "__main__":
    for text in get_new_post():
        print(text)
