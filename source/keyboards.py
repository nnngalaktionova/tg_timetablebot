from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup
from aiogram.types import KeyboardButton, ReplyKeyboardMarkup

main_keyboard_template = [
    [
        KeyboardButton("Расписание уроков📖"),
        KeyboardButton("Новости школы🗞"),
    ],
    [
        KeyboardButton("Наши учителя👨‍🏫"),
        KeyboardButton("Связь с разработчиками"),
    ],
]
main_keyboard = ReplyKeyboardMarkup(
    main_keyboard_template, resize_keyboard=True)

timetable_template = [
    [InlineKeyboardButton('11А', callback_data='timetable=11A')],
    [InlineKeyboardButton('11Б', callback_data='timetable=11B')],
    [InlineKeyboardButton('11B', callback_data='timetable=11C')],
    [InlineKeyboardButton('Я учитель', callback_data='timetable=teacher')],
]
timetable_teachers_template = [[InlineKeyboardButton('Кротова Татьяна Юрьевна', callback_data='timetable=teacher=01')],
                    [InlineKeyboardButton('Салтыкова Марина Викторовна ', callback_data='timetable=teacher=02')],
                    [InlineKeyboardButton('Алексанцева Дарья Владимировна', callback_data='timetable=teacher=03')],
                    [InlineKeyboardButton('Трисунова Наталья Викторовна', callback_data='timetable=teacher=04')],
                    [InlineKeyboardButton('Куляба Ирина Николаевна', callback_data='timetable=teacher=05')],]
teachers_template = [[InlineKeyboardButton('Бабушкин Борис Майевич', callback_data='teacher=0'),
                     InlineKeyboardButton('Белогорцева Елена Николаевна ', callback_data='teacher=1')],
                     [InlineKeyboardButton('Гончарова Елена Сергеевна', callback_data='teacher=2'),
                     InlineKeyboardButton('Елисеева Жанна Валерьевна'   , callback_data='teacher=3')],
                     [InlineKeyboardButton('Куляба Ирина Николаевна', callback_data='teacher=4'),
                     InlineKeyboardButton('Пылова Ирина Владимировна', callback_data='teacher=5')],
                     [InlineKeyboardButton('Россол-Завалейкова Мария Эдуардовна', callback_data='teacher=6'),
                     InlineKeyboardButton('Стукалова Татьяна Владимировна ', callback_data='teacher=7')],
                     [InlineKeyboardButton('Трисунова Наталья Викторовна', callback_data='teacher=8'),
                     InlineKeyboardButton('Фесенко Юлия Владимировна', callback_data='teacher=9')],
                     [InlineKeyboardButton('Хараим Олеся Леонидовна ', callback_data='teacher=10'),
                     InlineKeyboardButton('Желонкина Татьяна Серафимовна', callback_data='teacher=11')],
                     [InlineKeyboardButton('Попова Елена Анатольевна', callback_data='teacher=12'),
                     InlineKeyboardButton('Сиротина Татьяна Сергеевна', callback_data='teacher=13')],
                     [InlineKeyboardButton('Сургуч Наталья Анатольевна', callback_data='teacher=14'),
                     InlineKeyboardButton('Суханова Ольга Анатольевна', callback_data='teacher=15')],
                     [InlineKeyboardButton('Хацкевич Милана Владимировна', callback_data='teacher=16'),
                     InlineKeyboardButton('Алексанцева Дарья Владимировна', callback_data='teacher=17')],
                     [InlineKeyboardButton('Давигора Валерия Валентиновна ', callback_data='teacher=18'),
                     InlineKeyboardButton('Еремеева Ирина Алексеевна ', callback_data='teacher=19')],
                     [InlineKeyboardButton('Ильенко Марина Владимировна ', callback_data='teacher=20'),
                     InlineKeyboardButton('Кротова Татьяна Юрьевна', callback_data='teacher=21')],
                     [InlineKeyboardButton('Макренюк Ольга Анатолиевна ', callback_data='teacher=22'),
                     InlineKeyboardButton('Салтыкова Марина Викторовна', callback_data='teacher=23')],
                     [InlineKeyboardButton('Степаненко Елена Федоровна ', callback_data='teacher=24'),
                     InlineKeyboardButton('Чевын Майя Анатольевна', callback_data='teacher=25')],
                     [InlineKeyboardButton('Шахова Екатерина Андреевна', callback_data='teacher=26'),
                     InlineKeyboardButton('Артемчук Олеся Викторовна', callback_data='teacher=27')],
                     [InlineKeyboardButton('Елисеева Елена Богдановна', callback_data='teacher=28'),
                     InlineKeyboardButton('Задвицкая Анна Юрьевна', callback_data='teacher=29')],
                     [InlineKeyboardButton('Змиевская Анна Александровна', callback_data='teacher=30'),
                     InlineKeyboardButton('Козырева Ольга Николаевна', callback_data='teacher=31')],
                     [InlineKeyboardButton('Матюшкина Галина Михайловна ', callback_data='teacher=32'),
                     InlineKeyboardButton('Митин Виктор Георгиевич ', callback_data='teacher=33')],
                     [InlineKeyboardButton('Романова Инна Валентиновна', callback_data='teacher=34'),
                     InlineKeyboardButton('Скрябин Сергей Викторович ', callback_data='teacher=35')],
                     ]
timetable_keyboard = InlineKeyboardMarkup(inline_keyboard=timetable_template)
teachers_keyboard = InlineKeyboardMarkup(inline_keyboard=teachers_template)
timetable_teachers_keyboard = InlineKeyboardMarkup(inline_keyboard=timetable_teachers_template)