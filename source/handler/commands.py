import asyncio

import data.shedule
from source.bot import *
from aiogram import types
from datetime import datetime
from data import shedule
from data import texts
from source.keyboards import *
@dp.callback_query_handler()
async def new_button(event: types.CallbackQuery):
    id = event.from_user.id
    command = event.data
    print(id, command)

    if command.startswith('timetable'):
        if command.split('=')[1] != 'teacher':
            choice = command.split('=')[1]
            match choice:
                case '11A':
                    data1 = shedule.shedule
                case '11B':
                    data1 = shedule.shedule1
                case '11C':
                    data1 = shedule.shedule2
                case 'teacher':
                    data1 = shedule.shedule3
            weekday = datetime.now().weekday()
            if weekday <= 5:
                txt = "Расписание на сегодня:\n" + data1[weekday] + "\n\n"
                if weekday != 5:
                    txt += "Расписание на завтра:\n" + data1[weekday + 1]
            else:
                txt = "***Выходной***"
            await bot.send_message(id, txt, parse_mode='markdown')
        else:
            if command.count('=')==1:
                await bot.send_message(id,'Выберите своё имя', reply_markup=timetable_teachers_keyboard)
            if command.count('=')==2:
                choice = command.split('=')[2]
                await bot.send_message(id,data.shedule.shedule3[choice])
    if command.startswith('teacher'):
        choice = command.split('=')[1]
        await bot.send_message(id, texts.teachers[int(choice)])

