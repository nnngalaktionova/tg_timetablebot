import asyncio
from datetime import datetime

from data.shedule import *
from data.texts import *

from source.keyboards import *
from source.parser import *
from source.bot import *

from aiogram import types


@dp.message_handler()
async def new_message(message: types.Message):
    id = message.from_user.id
    text = message.text

    if "/start" == text:
        await bot.send_message(id, startup_message, reply_markup=main_keyboard)

    elif "Расписание уроков📖" == text:
        await bot.send_message(id, 'Выберите свой класс', parse_mode="markdown", reply_markup=timetable_keyboard)

    elif "Новости школы🗞" == text:
        for response in get_new_post():
            await bot.send_message(id, response, parse_mode="markdown")
++
    elif "Связь с разработчиками" == text:
        await bot.send_message(id, creater_message)
    elif "Наши учителя👨‍🏫" == text:
        await bot.send_message(id,'Выбирите учителя', reply_markup=teachers_keyboard)
